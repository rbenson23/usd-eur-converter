#!/usr/bin/env python3

"""
Author : paolo
Date   : =11-04-2022=
Purpose: Download currency value from European Central Bank writes the downloaded xml file and parses it. Convert the given value.
"""
import argparse
import requests
import os
import sys
import re

def get_args():
    """Get command-line arguments"""

    parser = argparse.ArgumentParser(
        description='Simple currency converter EUR/USD or viceversa',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-c',
                        '--currency',
                        help='whether to convert from Euro or USD',
                        type=str,
                        default='EUR')

    parser.add_argument('-a',
                        '--amount',
                        help='the amount to convert',
                        type=float,
                        default='1')
    
    return parser.parse_args()

def get_url(url):
    try:
        return requests.get(url)
    except:
        print(f"there was an error retrieving {url}")
        sys.exit()

def write_file(xml_file,res):        
    #try:
        with open(xml_file, "w") as f:
            f.write(res.text)
    # except:
    #     print(f"there was an error writing {xml_file}")
    #     sys.exit()

def parse_file(xml_file):
    #try:
        with open(xml_file, "r") as f:
            for line in f.readlines():
                if 'time' in line:
                    time = (re.findall('[0-9]{4}-[0-9]{2}-[0-9]{2}',line))
                if 'USD' in line:
                    value = (re.findall('[0-9].[0-9]{4}',line))
            value = value[0]
            time = time[0]
        return time, value
    #except:
    #    print(f"there was an error writing {xml_file}")
    #    sys.exit()

def caius_pp(s):
    ###caius_pretty_print, set a number to .3 decimal numbers 
    if s:
        return int(s *1000)/1000
    else:
        s = "None"
        return s

def print_output(res,time,value,currency,amount):
    print(f'Exchange rate date: {time}')
    print('--------------')
    print(f'1 EUR = {value}')
    print('--------------')
    if currency == 'EUR':
    # we have converted into EUR
        print(f'{amount} {currency} = {res} USD')
    else:
        print(f'{amount} {currency} = {res} EUR')
    
def main():

    args = get_args()

    # check what to convert, EUR or USD
    if args.currency == 'EUR':
        currency = args.currency
    elif args.currency == 'USD':
        currency = args.currency
    else:
        print('The only arguments allowed are EUR or USD')
        sys.exit()
        
    url = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"
    script_path = os.path.dirname(sys.argv[0])
    xml_file = script_path + "/currency-values.xml"
    res = get_url(url)
    write_file(xml_file, res)
    # return a tuple with two values: time, value
    b = parse_file(xml_file)
    time = b[0]
    value = b[1]

    if currency == 'EUR':
    # we have to convert amount to EUR
        res = caius_pp(float(args.amount)*float(value))
        print_output(res,time,value,currency,args.amount)
    else:
    # we have to convert to USD
        res = caius_pp(float(args.amount)/float(value))
        print_output(res,time,value,currency,args.amount)

# --------------------------------------------------

if __name__ == '__main__':
    main()
    


